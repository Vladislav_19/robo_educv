﻿namespace ur5.models
{
	//сообщения с дополнительными данными
    public class MessageExtraData
    {
        public bool warning = false;
        public bool error = false;
        public bool blocking = false;

        public MessageExtraData(bool warning, bool error, bool blocking)
        {
            this.warning = warning;
            this.error = error;
            this.blocking = blocking;
        }
    }
}