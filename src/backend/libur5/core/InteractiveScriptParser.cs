﻿namespace ur5.core
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using Sprache;
	
	//класс с ключевыми словами взаимодействия
    public class InteractKeywords
    {
        public static HashSet<string> ReservedWords { get; } =
            GetStrings(AllStringConstants.ToArray());

			//метода для удаление '(',')'
        void bla()
        {
            Parser<string> identifier =
                from first in Parse.Letter.Once()
                from rest in Parse.LetterOrDigit.Many()
                from bla1 in Parse.Char('(')
                from bla2 in Parse.Char(')')
                select new string(first.Concat(rest).ToArray());
        }

        #region Private

        private static IEnumerable<string> AllStringConstants =>
            typeof(InteractKeywords).GetTypeInfo().GetFields().Select(f => f.GetValue(null)).OfType<string>();
        private static HashSet<string> GetStrings(params string[] strings) =>
            new HashSet<string>(strings, StringComparer.OrdinalIgnoreCase);

        #endregion

		//объявление констант
        public const string Global = "global";
        public const string Call = "call";

        public const string True = "true";
        public const string False = "false";

        public const string As = "as";

        public const string Include = "include";


        // Types:
		
        public const string String = "string";
        public const string GUID = "guid";
        public const string Byte = "byte";
        public const string Int = "int";
        public const string Short = "short";
        public const string Unknown = "unknown";
        public const string Long = "long";
        public const string Bool = "bool";
        public const string Char = "char";
        public const string Float = "float";
        public const string Binary = "bin";
        public const string Gate = "gate";
        public const string Position = "position";
        public const string Void = "void";

    }
	//Интерактивный скрипт грамматики
    public class InteractiveScriptGrammar
    {
        protected internal virtual Parser<string> RawIdentifier =>
            from identifier in Parse.Identifier(Parse.Letter, Parse.LetterOrDigit.Or(Parse.Char('_')))
            where !InteractKeywords.ReservedWords.Contains(identifier)
            select identifier;
        protected internal virtual Parser<string> Identifier =>
            RawIdentifier.Token().Named("Identifier");

        protected internal virtual Parser<IEnumerable<string>> QualifiedIdentifier =>
            Identifier.DelimitedBy(Parse.Char('.').Token())
                .Named("QualifiedIdentifier");
        

        protected internal virtual Parser<string> Keyword(string text) =>
           Parse.IgnoreCase(text).Then(n => Parse.LetterOrDigit.Or(Parse.Char('_')).Not()).Return(text);

		//ключевые слова
        protected internal virtual Parser<TypeSyntax> SystemType =>
           Keyword(InteractKeywords.Bool).Or(
           Keyword(InteractKeywords.Byte)).Or(
           Keyword(InteractKeywords.Char)).Or(
           Keyword(InteractKeywords.Float)).Or(
           Keyword(InteractKeywords.GUID)).Or(
           Keyword(InteractKeywords.Int)).Or(
           Keyword(InteractKeywords.Long)).Or(
           Keyword(InteractKeywords.Unknown)).Or(
           Keyword(InteractKeywords.Short)).Or(
           Keyword(InteractKeywords.String)).Or(
           Keyword(InteractKeywords.Void))
               .Token().Select(n => new TypeSyntax(n))
               .Named("SystemType");
    }

	//введите синтаксис
    public class TypeSyntax
    {
		
        public bool IsArray { get; set; }
        public List<string> Namespaces { get; set; }
        public List<TypeSyntax> TypeParameters { get; set; }
        public string Identifier { get; set; }

        public TypeSyntax(string a)

        {

        }
        public TypeSyntax(IEnumerable<string> qualifiedName)
        {
            Namespaces = qualifiedName.ToList();

            if (Namespaces.Count > 0)
            {
                var lastItem = Namespaces.Count - 1;
                Identifier = Namespaces[lastItem];
                Namespaces.RemoveAt(lastItem);
            }
        }
        public string AsString() =>
           string.Join(".", Namespaces.Concat(Enumerable.Repeat(Identifier, 1))) +
               ((TypeParameters != null && TypeParameters.Count != 0) ? string.Empty :
                   "<" + string.Join(", ", TypeParameters.Select(t => t.AsString())) + ">") +
               (IsArray ? "[]" : string.Empty);
    }
	//основной синтаксис 
    public abstract class BaseSyntax
    {

        public abstract IEnumerable<BaseSyntax> ChildNodes { get; }
        

        protected IEnumerable<BaseSyntax> NoChildren => Enumerable.Empty<BaseSyntax>();

        public List<string> LeadingComments { get; set; } = new List<string>();

        public List<string> TrailingComments { get; set; } = new List<string>();

        private static Regex WhitespaceRegex { get; } = new Regex(@"\s+", RegexOptions.Compiled);
        
        

        public IEnumerable<BaseSyntax> DescendantNodes(Func<BaseSyntax, bool> descendIntoChildren = null) =>
            DescendantNodesAndSelf(descendIntoChildren).Skip(1);

        public IEnumerable<BaseSyntax> DescendantNodesAndSelf(Func<BaseSyntax, bool> descendIntoChildren = null)
        {
            yield return this;

            if (descendIntoChildren == null || descendIntoChildren(this))
            {
                foreach (var child in ChildNodes)
                {
                    foreach (var desc in child.DescendantNodesAndSelf(descendIntoChildren))
                    {
                        yield return desc;
                    }
                }
            }
        }
    }
}