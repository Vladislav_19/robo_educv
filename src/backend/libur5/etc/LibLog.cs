﻿namespace ur5.etc
{
    using System;
    using System.Collections.Generic;

	//
    public static class LibLog
    {
        private static readonly List<Type> SkipTypes = new List<Type>();
        public delegate void OnLogEvent(int code, object data);

        public static void Skip<T>()
        {
            if(!SkipTypes.Contains(typeof(T))) SkipTypes.Add(typeof(T));
        }

        private static event OnLogEvent _logEvent;
        public static void Error(object obj, object invoker = null)
        {
            if(invoker != null && SkipTypes.Contains(invoker.GetType()))
                return;
            _logEvent?.Invoke(3, obj);
        }

        public static void Info(object obj) => _logEvent?.Invoke(1, obj);

        public static void Trace(object obj) => _logEvent?.Invoke(0, obj);

        public static void Warn(object obj, object invoker = null) => _logEvent?.Invoke(2, obj);

        public static void RegisterHandle(OnLogEvent @event) => _logEvent += @event;
    }
}