﻿namespace ur5.etc
{
	//режим работы робота
    public enum RobotMode
    {
        NO_CONTROLLER = -1,
        RUNNING,
        FREEDRIVE,
        READY,
        INITIALIZING,
        SECURITY_STOPPED,
        EMERGENCY_STOPPED,
        FAUL,
        NO_POWER,
        NOT_CONNECTED,
        SHUTDOWN,
        SAFEGUARD_STOP
    }
}