﻿namespace urscript.interactive
{
    /// <summary>
    /// Implementation <see cref="SourceFile"/> for storage in memory
    /// </summary>
    public class SourceFileInMemory : SourceFile
    {
        public SourceFileInMemory(string source, string name) : base("<memory>")
        {
            Content = source;
            Name = name;
        }

        /// <summary>
        /// Name of file, without extension
        /// </summary>
        public override string Name { get; }
        /// <summary>
        /// File contents
        /// </summary>
        public override string Content { get; }
    }
}