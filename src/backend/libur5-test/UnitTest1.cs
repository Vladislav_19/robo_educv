namespace libur5_test
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Sprache;
    using ur5.core;
    using urscript.interactive;
    using Xunit;

    public class ParserTest : CorpusTest
    {
        [Fact]
        public void ParserFunctionWithArgsNumbers()
        {
            var result = TokenStorage.Function.Parse("function(1,2,3);");

            Assert.Equal("function", result.Identifier);
            Assert.Equal("1", result.Parameters.First());
            Assert.Equal("3", result.Parameters.Last());
        }

        [Fact]
        public void ParserFunctionWithArgsVariables()
        {
            var result = TokenStorage.Function.Parse("function(True,False,True);");

            Assert.Equal("function", result.Identifier);
            Assert.Equal("True", result.Parameters.First());
            Assert.Equal("True", result.Parameters.Last());
        }

        [Fact]
        public void ParserFunctionWithoutArgs()
        {
            var result = TokenStorage.Function.Parse("function();");

            Assert.Equal("function", result.Identifier);
            Assert.False(result.Parameters.Any());
        }
        [Fact]
        public void ParserFunctionCastPosition()
        {
            var result = TokenStorage.
                MoveFunction.
                Parse("[5.0, -2.0, 1.9, -2.9, -1.5, -0.0] when speed 12.2 when acc 10.2");

            Assert.Equal("12.2", result.Speed);
            Assert.Equal("10.2", result.Acceleration);
            Assert.Equal("5.0", result.Position.First());
            Assert.Equal("-0.0", result.Position.Last());
        }

        [Fact]
        public void ParserFunctionCastWithoutSpeed()
        {
            var result = TokenStorage.
                MoveFunction.
                Parse("[5.0, -2.0, 1.9, -2.9, -1.5, -0.0]");

            Assert.Null(result.Speed);
            Assert.Equal("5.0", result.Position.First());
            Assert.Equal("-0.0", result.Position.Last());
        }
        [Fact]
        public void ParserInclude()
        {
            var result = TokenStorage.
                Include.
                Parse("@include<'test'>() as Fragment;");
            
            Assert.True(result.FilesName.Any());
            Assert.Equal("test", result.FilesName.First());
        }

        [Fact]
        public void ParserIncludeMany()
        {
            var result = TokenStorage.
                Include.
                Parse("@include<'test', 'blad', 'setted'>() as Fragment;");

            Assert.True(result.FilesName.Any());
            Assert.Equal("test", result.FilesName.First());
            Assert.Equal("setted", result.FilesName.Last());
        }
        [Fact]
        public void IncludeCompileTest()
        {
            var module1 = new StringBuilder();

            module1.AppendLine("[-0.8161700000, -0.9686300000, -1.5230800000, -0.6746200000, 1.5930700000, -0.0211900000]");
            module1.AppendLine("MoveTool(0);");
            module1.AppendLine("MoveTool(255);");



            var ts = new StringBuilder();
            ts.AppendLine("[-0.8161700000, -0.1866700000, -1.8826400000, -1.0760400000, 1.5931400000, 0.0003200000]");
            ts.AppendLine("nt_move_tool(255)");
            ts.AppendLine("@include<'module1'>() as Fragment;");
            ts.AppendLine("nt_move_tool(0)");
            ts.AppendLine("[-0.8071100000, -1.9026800000, -1.1488800000, -0.0492200000, 1.5931400000, 0.0001700000]");

            SourceManager.DB.Add(new SourceFileInMemory(module1.ToString(), "module1"));


            URSc.Assembly(ts.ToString());
        }
        [Fact]
        public void CommentParseTest()
        {
            var module1 = new StringBuilder();
            
            module1.AppendLine("// haha beniz");
            module1.AppendLine("# haha beniz");
            module1.AppendLine("/* haha beniz */");
            module1.AppendLine("-- haha beniz");

            URSc.Assembly(module1.ToString());
        }
        [Fact]
        public void AdvancedCompileInclude()
        {
            var module0 = new StringBuilder();

            module0.AppendLine("MoveTool(12);");
            module0.AppendLine("MoveTool(13);");

            var module1 = new StringBuilder();

            module1.AppendLine("MoveTool(14);");
            module1.AppendLine("@include<'module0'>() as Fragment;");
            module1.AppendLine("MoveTool(15);");



            var mainModule = new StringBuilder();
            mainModule.AppendLine("MoveTool(16)");
            mainModule.AppendLine("@include<'module1'>() as Fragment;");
            mainModule.AppendLine("MoveTool(17)");

            SourceManager.DB.Add(new SourceFileInMemory(module0.ToString(), "module0"));
            SourceManager.DB.Add(new SourceFileInMemory(module1.ToString(), "module1"));


            var result = URSc.Assembly(mainModule.ToString());

            Assert.Contains("MoveTool(12)", result);
            Assert.Contains("MoveTool(13)", result);
            Assert.Contains("MoveTool(14)", result);
            Assert.Contains("MoveTool(15)", result);
            Assert.Contains("MoveTool(16)", result);
            Assert.Contains("MoveTool(17)", result);
        }

        [Fact]
        public void RecursionCompileInclude()
        {
            var module0 = new StringBuilder();

            module0.AppendLine("MoveTool(12);");
            module0.AppendLine("@include<'module1'>() as Fragment;");
            module0.AppendLine("MoveTool(13);");

            var module1 = new StringBuilder();

            module1.AppendLine("MoveTool(14);");
            module1.AppendLine("@include<'module0'>() as Fragment;");
            module1.AppendLine("MoveTool(15);");



            var mainModule = new StringBuilder();
            mainModule.AppendLine("MoveTool(16)");
            mainModule.AppendLine("@include<'module1'>() as Fragment;");
            mainModule.AppendLine("MoveTool(17)");

            SourceManager.DB.Add(new SourceFileInMemory(module0.ToString(), "module0"));
            SourceManager.DB.Add(new SourceFileInMemory(module1.ToString(), "module1"));
            Assert.Throws<AggregateException>(() =>
            {
                TimeoutAfter(new Task(() => { URSc.Assembly(mainModule.ToString()); }), 1000).Wait();
            });
        }

        [Fact]
        public void ComileTest4()
        {
            var module0 = new StringBuilder();

            module0.AppendLine("MoveTool(11);");

            var module1 = new StringBuilder();

            module1.AppendLine("MoveTool(12);");
            module1.AppendLine("#       @include<'module0'>() as Fragment;");
            module1.AppendLine("//@include<'module0'>() as Fragment;");
            module1.AppendLine("@include<'module0'>() as Fragment;");
            module1.AppendLine("@include<'module0'>() as Fragment;");
            module1.AppendLine("MoveTool(12);");

            var mainModule = new StringBuilder();
            mainModule.AppendLine("MoveTool(13)");
            mainModule.AppendLine("@include<'module1'>() as Fragment;");
            mainModule.AppendLine("@include<'module1'>() as Fragment;");
            mainModule.AppendLine("MoveTool(13)");

            SourceManager.DB.Add(new SourceFileInMemory(module0.ToString(), "module0"));
            SourceManager.DB.Add(new SourceFileInMemory(module1.ToString(), "module1"));


            //var result = URSc.Assembly(mainModule.ToString());
        }

        [Fact]
        public void ComileTest5()
        {
            var mainModule = new StringBuilder();
            mainModule.AppendLine("MoveTool(13)");
            mainModule.AppendLine("@include<'_module1'>() as Fragment;");
            mainModule.AppendLine("@include<'module1_'>() as Fragment;");
            mainModule.AppendLine("@include<'module-1'>() as Fragment;");
            mainModule.AppendLine("MoveTool(13)");

            SourceManager.DB.Add(new SourceFileInMemory("", "_module1"));
            SourceManager.DB.Add(new SourceFileInMemory("", "module1_"));
            SourceManager.DB.Add(new SourceFileInMemory("", "module-1"));

            var result = URSc.Assembly(mainModule.ToString());
        }
    }
}
