﻿namespace backend.Controllers
{
    using System;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc;
    using ur5.core;
    using ur5.models;
    using urscript.interactive;

    [Route("api/v1/ur5")]
    [ApiController]
    public class MonroController : ControllerBase
    {
        [Route("execute")]
        [HttpPost]
        public IActionResult Execute(ExecuteProgram model)
        {
            var result = SourceManager.SafeInvokeModule(model.ID);
            if(result == null)
                return Ok(new { status = 7 });
            if (getStatusForUID(MotherBoard.getUIDCurrentProgram()) != MonRoStatus.PROGRAM_HAS_COMPLETE)
                return Ok(new { status = 13 });
            if(URSc.TryAssembly(result, out var resu))
            {
                MotherBoard.SetNewProgram(new Program(model.ID, model.UID, new SourceFileInMemory(resu, model.ID)));
                MotherBoard.Play();
                return Ok(new { status = 1 });
            }
            return Ok(new { status = 5 });
        }
        [Route("status")]
        [HttpPost]
        public IActionResult Status(Root model)
        {
            var res = new {status = (int) getStatus(model), message = "", uid = MotherBoard.getUIDCurrentProgram()};
            return Ok(res);
        }

        private MonRoStatus getStatus(Root model)
        {
            switch (MotherBoard.globalState)
            {
                case GlobalState.NEED_REINIT_CONTROLLER:
                    return MonRoStatus.POWER_OFF;
                case GlobalState.FREEDRIVE:
                    return MonRoStatus.FREEDRIVE_ENABLED;
                case GlobalState.EVERGENCY_STOP:
                    return MonRoStatus.EMERGENCY_STOP;
                case GlobalState.SECURITY_STOP:
                    return MonRoStatus.SECURITY_STOP;
            }

            if (model.UID == "000000")
                return getStatusForUID(MotherBoard.getUIDCurrentProgram());

            if (string.IsNullOrEmpty(model.UID))
                return MonRoStatus.INVALID_UID;

            return getStatusForUID(model.UID);
        }

        private MonRoStatus getStatusForUID(string uid)
        {
            if (string.IsNullOrEmpty(uid))
                return MonRoStatus.PROGRAM_HAS_COMPLETE;
            var prog = MotherBoard.ProgramStack.FirstOrDefault(x => x.UID == uid);
            if (prog == null)
                return MonRoStatus.INVALID_UID;
            switch (prog.Status)
            {
                case ProgramStatus.NonStarted:
                    return MonRoStatus.SECURITY_STOP;
                case ProgramStatus.Playing:
                    return MonRoStatus.PROGRAM_RUNNED;
                case ProgramStatus.FailedToPlay:
                    return MonRoStatus.SECURITY_STOP;
                case ProgramStatus.Paused:
                    return MonRoStatus.PROGRAM_PAUSED;
                case ProgramStatus.Ended:
                    return MonRoStatus.PROGRAM_HAS_COMPLETE;
                case ProgramStatus.UnknownError:
                default:
                    return MonRoStatus.SECURITY_STOP;
            }
        }
    }

    public class Root
    {
        public string UID { get; set; }
    }
    public class ExecuteProgram : Root
    {
        public string ID { get; set; }
    }

    public enum MonRoStatus
    {
        PROGRAM_HAS_COMPLETE = 0,
        PROGRAM_STARTED = 1,
        PROGRAM_RUNNED = 2,
        PROGRAM_PAUSED = 3,
        POWER_OFF = 4,
        INVALID_PROGRAM = 5,
        SECURITY_STOP = 6,
        INVALID_UID = 7,
        UNUSED_1 = 8,
        EMERGENCY_STOP = 9,
        REQUEST_REBOOT = 10,
        UNUSED_2 = 11,
        FREEDRIVE_ENABLED = 12,
        PROGRAM_ALREADY_RUNNED = 13,
    }
}