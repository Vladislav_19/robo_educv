Document.prototype.ready = function(callback) {
    if (callback && typeof callback === 'function') {
        document.addEventListener("DOMContentLoaded", function() {
            if (document.readyState === "interactive" || document.readyState === "complete") {
                return callback();
            }
        });
    }
};
// usage
document.ready(function() {
    function sleep(time) {
        return new Promise((resolve) => setTimeout(resolve, time));
    }

    function clearIngredients() {
        // сбрасываем предыдущие данные ингредиентов
        var currentCocktail = null;
        document.getElementById("b-message--done").style.backgroundImage = "";
        document.querySelectorAll('.b-ingredients__item--active').forEach(function(el) {
            el.classList.remove('b-ingredients__item--active');
        })
        document.querySelectorAll('.b-ingredient__volume').forEach(function(el) {
            el.classList.innerHTML = '';
        })
        // сбрасываем предыдущие данные ингредиентов
    }
    // адрес где брать данные для заполнения
    var jsonDataSource = '/data.json';
    jsonDataRequest = new Request(jsonDataSource);
    axios.get(jsonDataSource).then(function(response) {
        var dataJson = response.data;
        // контейнер для названия текущего коктейля
        // коктейли    
        var templateCocktails = document.getElementById("cocktails-template");
        template = Handlebars.compile(templateCocktails.innerHTML);
        templateCocktails.innerHTML = template(dataJson);
        templateCocktails.style.display = "block";
        // ингредиенты 
        var templateIngredients = document.getElementById("ingredients-template");
        template = Handlebars.compile(templateIngredients.innerHTML);
        templateIngredients.innerHTML = template(dataJson);
        // templateIngredients.style.display="block";
        // сообщение об успешном завершении
        var templateMsgDone = document.getElementById("message-done");
        template = Handlebars.compile(templateMsgDone.innerHTML);
        templateMsgDone.innerHTML = template(dataJson);
        // templateMsgDone.style.display="block";
        // сообщение об ошибке
        var templateMsgError = document.getElementById("message-error");
        template = Handlebars.compile(templateMsgError.innerHTML);
        templateMsgError.innerHTML = template(dataJson);
        // templateMsgError.style.display="block";
        function showErrorMsg() {
            templateCocktails.style.display = "none";
            templateIngredients.style.display = "none";
            templateMsgDone.style.display = "none";
            templateMsgError.style.display = "block";
            // очищаем данные предыдущего коктейля
            clearIngredients();
        }

        function showMainScreen() {
            templateIngredients.style.display = "none";
            templateMsgDone.style.display = "none";
            templateMsgError.style.display = "none";
            templateCocktails.style.display = "block";
            // очищаем данные предыдущего коктейля
            clearIngredients();
        }

        function showIngredientsScreen(currentCocktail) {
            var elementMsgImg = document.getElementById("b-message--done");
            elementMsgImg.style.backgroundImage = "url(" + currentCocktail.icon_l + ")";
            templateIngredients.style.display = "none";
            templateMsgDone.style.display = "block";
            // возвращаемся на Главный экран по таймауту в 3 секунды
            sleep(3000).then(() => {
                templateMsgDone.style.display = "none";
                templateCocktails.style.display = "block";
                // очищаем данные предыдущего коктейля
                clearIngredients();
            });
        }
        // вешаем эвент клика на все блоки коктейлей
        document.addEventListener('click', function(e) {
            // If the clicked element doesn't have the right selector, bail
            if (!e.target.matches('.b-cocktail')) return;
            // Don't follow the link
            e.preventDefault();
            // status = 1 - 1 = error
            // небольшая задержка, чтобы прошла CSS анимация тача по кнопке
            sleep(350).then(() => {
                // идентификатор коктейля
                var key = e.srcElement.name;
                var currentCocktail = dataJson.cocktails[key];
                // исключение для red bull - он не готовится, для него не показываем игридиенты
                if (key == 'red-bull') {
                    templateCocktails.style.display = "none";
                    templateIngredients.style.display = "none";
                    var elementMsgImg = document.getElementById("b-message--done");
                    elementMsgImg.style.backgroundImage = "url(" + currentCocktail.icon_l + ")";
                    templateMsgDone.style.display = "block";
                } else {
                    _ingredients = currentCocktail.ingredients;
                    if (_ingredients) {
                        templateCocktails.style.display = "none";
                        templateIngredients.style.display = "block";
                        var elementIngredientsTitle = document.getElementById("b-ingredients__title");
                        elementIngredientsTitle.innerHTML = currentCocktail.nameForProgress;
                        for (var ingredient in _ingredients) {
                            if (_ingredients.hasOwnProperty(ingredient)) {
                                activeIngredient = document.getElementById("item-" + ingredient);
                                activeLine = document.getElementById("line-" + ingredient);
                                activeVolume = document.getElementById("volume-" + ingredient);
                                activeVolume.innerHTML = _ingredients[ingredient];
                                activeIngredient.classList.add('b-ingredients__item--active');
                            }
                        }
                    }
                }
                // отправляем запрос на приготовление коктейля
                axios.post('/api/v1/command', { name: key }).then(function(response) {
                    // handle success
                    console.log(response);
                    // пришел ответ =1, ставим на лонг поллинг
                    if (response.data.status == '0') {
                        // long polling
                        var longPolling = setInterval(function() {
                            // обращение к API по клику на коктейль
                            // вызов API робота по идентификатору коктейля
                            axios.get('/api/v1/status/?key=' + key).then(function(response) {
                                // success code = 1
                                if (response.data.status == '1') {
                                    clearTimeout(longPolling);
                                    showIngredientsScreen(currentCocktail);
                                }
                                // error 
                                if (response.data.status == '-1') {
                                    clearTimeout(longPolling);
                                    showErrorMsg();
                                }
                                // третий вариант ДЛЯ ПРИМЕРА
                                if (response.data.status == '0') {
                                    clearTimeout(longPolling);
                                    showMainScreen();
                                }
                            }).catch(function(error) {
                                console.log(error);
                            });
                        }, 1000);
                    }
                    // error 
                    if (response.data.status != '0') {
                        showErrorMsg();
                    }
                }).catch(function(error) {
                    // handle error
                    console.log(error);
                    // делаем что-то при ERROR ответе
                    showErrorMsg();
                });
            }, false); // eof sleep 
        });
    });
});