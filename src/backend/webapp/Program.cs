﻿namespace backend
{
    using System;
    using CLIMapper;
    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Logging;
    using RC.Framework.Screens;
    using ur5.etc;
    using urscript.interactive;

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "UR Phoenix v1.6.0";
            RCL.EnablingVirtualTerminalProcessing();
            SourceManager.IndexingSourceFiles("./db");
            LibLog.RegisterHandle((code, data) =>
            {
                Screen.WriteLine(data.ToString());
            });
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var cmds = Mapper.Map(args);
            return WebHost.CreateDefaultBuilder(args).UseKestrel(x =>
                {
                    var port = cmds.ContainsKey("port") ? int.Parse(cmds["port"].ToString()) : 1337;
                    x.ListenAnyIP(port);
                    if (!cmds.ContainsKey("enable-logging"))
                    {
                        Screen.WriteLine("Phoenix has started.");
                        Screen.WriteLine($"Listern on 'localhost:{port}'");
                    }
                })
                .ConfigureLogging(config =>
                {
                    if(!cmds.ContainsKey("enable-logging"))
                    config.ClearProviders();
                })
                .UseStartup<Startup>();
        }
            
    }
}