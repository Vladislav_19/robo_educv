#tool "nuget:?package=NUnit.ConsoleRunner"
#tool "nuget:?package=xunit.runner.console"
#addin "Cake.Yarn"


var target = Argument("target", "Default");
var configuration = Argument("configuration", "Debug");
var solution = ("./src/backend/ur5-platform.sln");
string findFile(string path, string pattern)
{
    return System.IO.Directory.GetFiles(path, pattern).First();
}
Task("Clean")
    .Does(() =>
{
  CleanDirectory($"./artifact");
});

Task("Restore")
    .Does(() =>
{
    DotNetCoreRestore(solution, new DotNetCoreRestoreSettings
    {
        Verbosity = DotNetCoreVerbosity.Minimal,
        Sources = new [] { "https://api.nuget.org/v3/index.json" }
    });
});

Task("Build")
    .Does(() =>
{
    // Build the solution.
    var path = MakeAbsolute(new DirectoryPath(solution));
    DotNetCoreBuild(path.FullPath, new DotNetCoreBuildSettings()
    {
        Verbosity = DotNetCoreVerbosity.Minimal,
        Configuration = configuration,
        NoRestore = true
    });
});

Task("Publish")
    .Does(() =>
{
    DotNetCorePublish("./src/backend/webapp/backend.csproj", new DotNetCorePublishSettings
    {
        Verbosity = DotNetCoreVerbosity.Minimal,
        NoRestore = true,
        Configuration = configuration,
        OutputDirectory = $"./artifact/phoenix-backend"
    });
    DotNetCorePublish("./src/backend/CLI/CLI.csproj", new DotNetCorePublishSettings
    {
        Verbosity = DotNetCoreVerbosity.Minimal,
        NoRestore = true,
        Configuration = configuration,
        OutputDirectory = $"./artifact/UR5-CLI"
    });
    CopyDirectory("./src/backend/webapp/Views", "./artifact/phoenix-backend/Views");
});

Task("Yarn-Install")
  .Does(() =>
{
    Information("Restoring vscode.ursharp.extension...");
    Yarn.FromPath("./src/vscode-ext/ursharp").Install();
});

Task("Build-VisualCode-Packages")
  .Does(() =>
{
  Information("Building webapp...");
  Yarn.FromPath("./src/vscode-ext/ursharp").RunScript("build");
  Yarn.FromPath("./src/vscode-ext/ursharp").RunScript("pkg");
  var vsix = findFile("./src/vscode-ext/ursharp", "*.vsix");
  var fileVsix = System.IO.Path.GetFileName(vsix);
  CopyFile(vsix, $"./artifact/{fileVsix}");
});



Task("Test")
  .Does(() =>{
    var settings = new DotNetCoreTestSettings
    {
      Verbosity = DotNetCoreVerbosity.Minimal,
    };
    DotNetCoreTest("./src/backend/libur5-test/libur5-test.csproj", settings);
  });

Task("Documentation")
  .Does(() =>{
      CreateDirectory("./artifact/doc");
      CopyFile("./src/backend/webapp/api.md", "./artifact/doc/front-api.md");
      CopyFile("./src/backend/webapp/modbus_base.rb", "./artifact/doc/modbus_base.rb");
      CopyFile("./src/backend/libursc/specification.md", "./artifact/doc/spec_ursharp.md");
      CopyFile("./src/backend/CLI/README.md", "./artifact/doc/cli.md");
  });
Task("Default")
    .IsDependentOn("Clean")
    .IsDependentOn("Restore")
    .IsDependentOn("Build")
    .IsDependentOn("Publish")
    .IsDependentOn("Yarn-Install")
    .IsDependentOn("Build-VisualCode-Packages")
    .IsDependentOn("Documentation")
    //.IsDependentOn("Test") // wtf, in debug - passed. in normal start - loop
    ;

RunTarget(target);